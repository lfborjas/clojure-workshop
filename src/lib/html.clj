(ns lib.html)

(declare flatten-strs
         compile-html)

(defmacro html
  [& content]
  (flatten-strs `(str ~@(compile-html content))))

(defmacro html-unflat
  [& content]
  `(str ~@(compile-html content)))


(comment
  (macroexpand '(html-unflat
                [:div {:id "first"}
                 [:span "hello"]
                 [:h1 {:class "title"} "Title"]
                 [:ul
                  (for [x (range 1 4)]
                    [:li x])]]))
  (html [:div {:id "first"}
         [:span "hello"]
         [:h1 {:class "title"} "Title"]
         [:ul
          (for [x (range 1 4)]
            [:li x])]])
  (macroexpand-1
   '(html [:div {:id "first"}
           [:span "hello"]
           [:h1 {:class "title"} "Title"]
           [:ul
            (for [x (range 1 4)]
              [:li x])]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HELPERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Taken almost verbatim from an early version of hiccup:
;; https://github.com/weavejester/hiccup/blob/c340657d0ac484bb9f59c4b3df0aff8931e9d514/src/hiccup/core.clj

(defn- flatten-strs
  "Collapse nested str expressions into one, where possible."
  [expr]
  (if (seq? expr)
    (cons
     (first expr)
     (mapcat
      #(if (and (seq? %) (= (first %) (first expr) `str))
         (rest (flatten-strs %))
         (list (flatten-strs %)))
      (rest expr)))
    expr))

(comment
  ;; note that this works on actual code
  ;; ` -> syntax quote: https://blog.klipse.tech/clojure/2016/05/05/macro-tutorial-3.html
  ;;  or: https://cljs.github.io/api/syntax/syntax-quote
  (flatten-strs `(str (str "1" (str "2" "3")))))

(defn- uneval?
  "Is this an unevaluated form/symbol?"
  [x]
  (or (symbol? x)
      (and (seq? x)
           (not= (first x) `quote))))

(comment
  (uneval? `x)
  (uneval? 'x)
  (uneval? {:a :map})
  (uneval? (quote x))
  (uneval? '(1 2 3))
  (uneval? `(str "a"))
  (uneval? `(quote x)))

(defn- literal?
  "Can this value be rendered as-is?"
  [x]
  (and (not (uneval? x))
       (or (not (vector? x))
           (every? literal? x))))

(comment
  (literal? "2"))

(defn- format-attr
  "Turn a name/value pair into an attr string"
  [n value]
  (str " "
       (name n)
       "=\""
       ;; hiccup does some html escaping here:
       (name value)
       "\""))

(comment
  (format-attr :a 42))

(defn- make-attrs
  "Turn a map into a string of html attrs"
  [attrs]
  (apply str
         (sort
          (for [[attr value] attrs]
            (cond
              ;; deals with standalone attrs, like `checked`
              (true? value)
              (format-attr attr attr)
              
              (not value) 
              ""
              
              :else
              (format-attr attr value))))))

(comment
  (make-attrs {:id "foo" :checked true}))

(declare render-html)

(defn- render-tag
  "Render an HTML tag"
  [[tag* & others]]
  (let [tag (name tag*)
        attrs* (first others)
        attrs (if (map? attrs*) attrs* {})
        content (if-not (map? attrs*) others (next others))]
    (if (some? content)
      (str "<" tag (make-attrs attrs) ">"
           (render-html content)
           "</" tag ">")
      (str "<" tag (make-attrs attrs) " />"))))

(defn render-html
  "Render a data structure into an HTML string"
  [data]
  (cond
    (vector? data)
    (render-tag data)
    
    (seq? data)
    (apply str (map render-html data))
    
    :else
    (str data)))

(defn- compile-lit-tag+attrs
  "Compile an element given its tag and attrs"
  [tag* attrs content]
  (let [tag (name tag*)]
    (if (some? content)
      `(str ~(str "<" tag (make-attrs attrs) ">")
            ~@(compile-html content)
            ~(str "</" tag ">"))

      `(str ~(str "<" tag)
            (make-attrs attrs)
            "/>"))))

(defn- compile-tag
  "Pre-compile a single-tag vector"
  [[tag attrs & content :as element]]
  (cond
    ;; parses [:span "hello"]
    (every? literal? element)
    (render-tag (eval element))
    
    ;; parses [:input {:type "text"} x]
    (and (literal? tag) (map? attrs))
    (compile-lit-tag+attrs tag attrs content)
    
    ;; parses [:div x]
    (literal? tag)
    (compile-lit-tag+attrs tag {} (cons attrs content))
    
    :else
    `(render-tag
      [~(first element)
       ~@(for [x (rest element)]
           (if (vector? x)
             (compile-tag x)
             x))])))

(comment
  (literal? :div)
  (literal? [])
  (every? literal? '[:span "hello"])
  (compile-tag '[:span "hello"])
  ;; NOTE(luis) does this return code, or a string?
  ;; try `eval` and `every? literal?`:
  (compile-tag
   '[:div {:id "first"}
     [:span "hello"]
     [:h1 {:class "title"} "Title"]])
  ;; returns code:
  (compile-tag '[:div (identity 4)]))

(defn- compile-html
  [content]
  (for [c content]
    (cond
      (vector? c)
      (compile-tag c)

      (literal? c)
      c

      :else `(render-html ~c))))
