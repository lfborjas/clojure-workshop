(ns lib.routes 
  (:require
   [clojure.spec.alpha :as s]
   [clojure.walk :refer [macroexpand-all]]))

(defn- make-routes-handler
  "Create a Ring handler from several route definitions"
  [& handlers]
  (fn [request]
    ;; returns the first response that
    ;; successfully handled the request
    (some #(% request)
          handlers)))

(defn- method-matches?
  [{:keys [request-method]} method]
  (= request-method method))

(defn- path-matches?
  [{:keys [uri path-info]} path]
  (= (or uri path-info) path))

(defn- make-route
  [method path handler]
  (fn [request]
    (when (and (method-matches? request method)
               (path-matches?   request path))
      (handler request))))

(defn- valid-request?
  [{param-specs :params} {:keys [params]}]
  (s/valid? param-specs params))

(defn- compile-route
  "Turn a route definition into a function"
  [method path specs handler]
  `(make-route
    ~method
    ~path
    (fn [request#]
      (if (and (seq ~specs)
               (not (valid-request? ~specs request#)))
        {:status 422 :body "Invalid request"}
        (~@handler request#)))))

(defmacro defroutes
  [name & routes]
  `(def ~name (make-routes-handler ~@routes)))

(defmacro GET
  [path specs & handler]
  (compile-route :get path specs handler))

(defmacro POST
  [path specs & handler]
  (compile-route :post path specs handler))


(comment
  (macroexpand '(and 1 2))
  (macroexpand
   '(GET "/tasks"
      {:params ::spec}
      handler))
  (macroexpand-all
   '(defroutes app
      (GET "/tasks"
        {:params ::spec}
        handler)
      (POST "/tasks"
        {} handler2)))
  
  ;; NOTE(luis)
  ;; try using the debugger: can introduce a breakpoint manually
  ;; but the instrumented form _must_ be evaluated (not the whole)
  ;; file; and _then_ this form
  #_(do 
    (s/def ::message #(> (count %) 0))
    (defroutes app
     (GET "/tasks"
       {}
       (fn [_] {:body "get!"}))
     (POST "/tasks"
       {:params (s/keys :req-un [::message])}
       (fn [_] {:bdy "post!"})))
   (app {:request-method :post
         :uri "/tasks"
         :params {:message ""}})))
