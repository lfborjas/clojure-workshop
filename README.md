# clj-workshop/time-tracker

Example web application 

* Uses Reitit for routing
* Uses Hiccup for HTML
* Uses Datomic for the data layer

There's a few branches with examples + exercises for each topic. Check out [the wiki](https://gitlab.com/lfborjas/clojure-workshop/-/wikis/home).

## Usage

We use `deps.edn` and `tools.build` for dependencies and building tasks,
respectively.

Note that the `clj` CLI tool is a wrapper over the `clojure` executable,
adding some dependencies that make it friendlier for dev.

### Run the application

```sh
clj -M:run
```

The `-M` option will compile and run a `main`, as configured in the alias in `deps.edn`

You can also run the application in dev mode, which will reload the server whenever any
files in the source tree are changed:

```sh
clj -M:dev -m clj-workshop.dev-server
```

### Run tests

```sh
clj -X:test
```

The `-X` option will execute as configured in the respective alias in `deps.edn`.


### Create an uberjar

```sh
clj -T:build uber
```

The `-T` option calls a tool by name, as configured in the respective alias.

Will create a standalone `jar` in the `target` folder; as specified in the
`uber` task of the `build.clj` file (which in turn is configured via `deps.edn`

The jar can then be run with the regular java machinery (`java -jar target/...`)
